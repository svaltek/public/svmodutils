--[[-------------------------------------------------------------------------------------
    SvModUtils - Usefull Functions
--]]-------------------------------------------------------------------------------------
local SvModUtils = {}


-----------------------------------------------------------------------------------------
--Internals
local myLog = function(text)
  Log("[SvMu]" .. text)
end

local function prequire(m) 
  local ok, err = pcall(require, m) 
  if not ok then return nil, err end
  return err
end


-- Writes a file to disk
-- @filename - name/path of file to Write, starts in Server root
-- @fData - File Contents to Write
local function SvWriteFile(filename, fData)
  file = io.open(filename, "w")
  file:write(fData)
  file:close()
end

-- Reads in a file from disk
-- @filename - name/path of file to Write, starts in Server root
local function SvLoadFile(filename)
  package.path = "./" .. filename .. ";" .. package.path
  local theFile = require(filename)
  return theFile
end

-- Write table to luafile on disk
-- @filename - name/path of file to Write, starts in Server root
-- @fData the - table to write
local function SvWriteConfig(filename, fData)
  file = io.open(filename, "w")
  file:write(fData)
  file:close()
end

-- load table from lua file on disk
-- You must provide a default table to use/write if not found
-- @filename - name/path of file to Write, starts in Server root
-- @fData - default table to use and write when file not found
local function SvReadConfig(filename, defaultConfig)
  local thisConfig = {}
  local file = io.open(filename)
  if file == nil then
    SvWriteConfig(filename, defaultConfig)
    return defaultConfig
  end
  file:close()
  package.path = "./" .. filename .. ";" .. package.path
  local settings = require(filename)
  for a, b in pairs(settings) do
    thisConfig[a] = b
  end
  return thisConfig
end

-- Require a Module relative to the current one
local SvRequireRelative
if arg and arg[0] then
  package.path = arg[0]:match("(.-)[^\\/]+$") .. "?.lua;" .. package.path
  SvRequireRelative = require
elseif ... then
  local d = (...):match("(.-)[^%.]+$")
  function SvRequireRelative(module)
    return require(d .. module)
  end
end

--- Check if a file or directory exists in this path
local function SvExists(file)
  local ok, err, code = os.rename(file, file)
  if not ok then
    if code == 13 then
      -- Permission denied, but it exists
      return true
    end
  end
  return ok, err
end

-----------------------------------------------------------------------------------------
-- Remap Internal Functions
SvModUtils.ReadFile = SvLoadFile
SvModUtils.WriteFile = SvWriteFile
SvModUtils.ReadConfig = SvReadConfig
SvModUtils.WriteConfig = SvWriteConfig
SvModUtils.requireRelative = SvRequireRelative

-----------------------------------------------------------------------------------------
--Main Functions

-- Log Wrapper for default Print function
-- @fData - log content
function SvModUtils.Log(fData)
  Log(fData)
end

-- Merge Table B onto Table A Overwriting Existing Keys
function SvModUtils.mergeTables(TableA, TableB)
  if type(a) == "table" and type(b) == "table" then
    for k, v in pairs(TableB) do
      if type(v) == "table" and type(TableA[k] or false) == "table" then
        merge(TableA[k], v)
      else
        TableA[k] = v
      end
    end
  end
  return TableA
end

-- Check if a file exists in path
function SvModUtils.isFile(path)
  return SvExists(path)
end

-- Check if a directory exists in path
function SvModUtils.isDir(path)
  return SvExists(path .. "/")
end

-- Create a folder at Path
function SvModUtils.mkDir(fPath)
  if not SvModUtils.isDir(fPath) then
    myLog("Directory: " .. fPath .. " Does not Exist, Creating....")
    local thisResult = os.execute("mkdir " .. fPath)
    if thisResult == err then
      myLog("Failed to Create " .. fPath .. " Directory!")
    else
      myLog("Successfully Created " .. fPath .. " Directory!")
    end
    return thisResult
  end
end

----------------------------------------------------------------  
-- Pretty Print a Table
-- Converts a table to Human readable text supports full recursion
-- This is a wrapper around inspect supporting all  of inspects options
-- Check here for more info:https://github.com/kikito/inspect.lua
-- @fTable - the table to Print
-- @fOptions - options to pass to Inspect
function SvModUtils.DebugTable(fTable, fOptions)
  -- if the inspect module is available
  local inspect = prequire('SvServerMods.lualibs.inspect')
  if inspect then
  local fResult = inspect(fTable, fOptions)
  return fResult
  else
    myLog('DebugTable requires the Inspect Library to be loaded')
  end
end

----------------------------------------------------------------
-- for debugging - redeclare Log as print when not ingame/server
if Log == nil then
  Log = print
  return SvModUtils
else
  SvMu = SvModUtils
end
