# SvMu - SvModUtils

A Collection of useful Functions for mod development

  Workshop ID : [1750069108]()

This is Currently Under Development as Such some things may not allways work as intended and Features may be added or removed at any time.



<ins>How to Use:</ins>

This Mod wont do anything on it's own...
Installing this mod will provide a new global namespace `SvMu` with various functions you might find usefull for mod creation

Use `SvMu.{someFunction}` to use the various functions provided.

###### Example:

```lua
local defaultdictionary = {hello = "used to express a greeting ", world = "the earth or globe, considered as a planet"}

--Check if folder exists in server root if not then create it
if not SvMu.isDir('Dictionary') then
    SvMu.mkDir('Dictionary')
end

-- Load a config file (lua table) from disk starting at server root and write a default one if not exist
local dictionary = SvMu.ReadConfig('Dictionary/Dictionary.lua',defaultdictionary)

--Use SvMu.DebugTable to "Pretty Print" a table
-- and output it to log using SvMu.Log to fallback to print when debugging in ide
SvMu.Log(SvMu.DebugTable(dictionary))

```



#### Current Functions:

```lua
Log(fData)
-- Log Wrapper for default Print function
-- This Is usefull in mod development to allow
-- ide/debugger fallback to standard print function
-- @fData - log content

DebugTable(fTable,fOptions)
-- Pretty Print a Table
-- Converts a table to Human readable text supports full recursion
-- This is a wrapper around inspect supporting all  of inspects options
-- Check here for more info: https://github.com/kikito/inspect.lua
-- @fTable - the table to Print
-- @fOptions - options to pass to Inspect

mergeTables(TableA,TableB)
-- Merge Table B onto Table A Overwriting Existing Keys

WriteFile(filename,fData)
-- Writes a file starting from Server Root
-- @filename - name/path of file to Write
-- @fData - File Contents to Write

LoadFile(filename)
-- Reads in a file starting from Server Root
-- @filename - name/path of file to Write

WriteConfig(filename,fData)
-- Write table to luafile on disk
-- @filename - name/path of file to Write
-- @fData the - table to write

ReadConfig(filename,defaultConfig)
-- loads a table from a lua file on disk
-- You must provide a default table to use/write if not found
-- @filename - name/path of file to Write
-- @fData - default table to use and write when file not found

requireRelative(module)
-- Require a Module relative to the current one
-- @module module to load

isFile(path)
-- Check if a directory exists at path

isDir(path)
-- Check if a directory exists at path

mkDir(path)
-- Create a Directory at path if it doesn't allready exist
```

######  Issues / Submissions

Users are Welcome to Suggest additions/changes to this project, but please 
follow the [Issues Template](ISSUE_TEMPLATE.md)